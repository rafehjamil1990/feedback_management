class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :description
      t.string :type
      t.integer :create_by_id, null: false

      # for awesome nested set
      t.integer :parent_id, null: true, index: true
      t.integer :lft, null: true, index: true
      t.integer :rgt, null: true, index: true

      # optional fields
      t.integer :depth, null: false, default: 0
      t.integer :children_count, null: false, default: 0

      t.timestamps
    end
  end
end
