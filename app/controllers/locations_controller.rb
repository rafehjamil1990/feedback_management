class LocationsController < ApplicationController
  load_and_authorize_resource

  # GET /locations
  def index
    @locations = @locations.paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html
    end
  end
  
  # GET /locations/:id
  def show
    respond_to do |format|
      format.html
    end
  end

  # GET /locations/new
  def new
    @locations = Location.all
    respond_to do |format|
      format.html
    end
  end

  # POST /locations
  def create
    @location.save
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  # GET /locations/:id
  def edit
    respond_to do |format|
      format.html
    end
  end

  # PUT/PATCH /locations/:id
  def update
    respond_to do |format|
      format.html
    end
  end

  # DELETE /locations/:id
  def destroy
    respond_to do |format|
      format.html
    end
  end

  private

  def location_params
    params.require(:location).permit(:name, :description, :type, :parent_id, :created_by_id)
  end

end
