class UsersController < ApplicationController
  before_action :authenticate_user!, :side_bar_menu
  load_and_authorize_resource

  # GET /users
  def index
    @sub_tree_menu = 'index'
    @users = @users.paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html
    end
  end

  # GET /users/:id
  def show
    respond_to do |format|
      format.html
    end
  end

  # GET /users/new
  def new
    @sub_tree_menu = 'index'
    respond_to do |format|
      format.html
    end
  end

  # POST /users
  def create
    respond_to do |format|
      format.html { redirect_to :index }
    end
  end

  # PUT/PATCH /users/:id
  def edit
  end

  # DELETE /users/:id
  def destroy
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :mobile, :address, :avatar)
  end

  def side_bar_menu
    @tree_menu = 'users'
  end
end
