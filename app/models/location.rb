class Location < ApplicationRecord
  acts_as_nested_set
  self.inheritance_column = nil
  belongs_to :created_by, class_name: User.to_s
  belongs_to :parent, class_name: Location.to_s

  LOCATION_TYPES = {
    division: 'Division',
    district: 'District',
    tehsil: 'Tehsil',
    union_council: 'Union Council'
  }.freeze

end
